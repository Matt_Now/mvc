<?php

include '../Model/bdd.php';

$bdd = New bdd();
if(!isset($_GET['categorie'])){
    $categorie = $bdd->fetchCategorie();

   include '../View/categorie.php';
} elseif($_GET['categorie'] == 'getOneById'){

    $id = $_GET['id'];

    $categorie = $bdd->fetchCategorieById($id);

    include '../View/categorieDetail.php';

} elseif ($_GET['categorie'] == 'delete'){
    $id = $_GET['id'];
    $bdd->deletecategorie($id);;
    header('Location: ../Controller/categorie.php');
} elseif ($_GET['categorie'] == 'add') {
    $id = $_POST['id'];
    $nom = $_POST['nom'];
    $descrip = $_POST['descrip'];
    $bdd->addCategorie($id, $nom, $descrip);
    header('Location: ../Controller/categorie.php');
} elseif ($_GET['categorie'] == 'beforeAdd') {
    $categories = $bdd->fetchCategorie();
    include ('..\View\newCategorie.php');}