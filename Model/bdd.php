<?php

class bdd{
    private $bdd;


public function __construct(){
      try{
           $this->bdd = new PDO('mysql:host=localhost;dbname=coursmvc;charset=utf8', 'root', '');

    }catch(PDOException $e){
            print($e);
      }
}


public function fetchArticles(){
    $req = $this->bdd->prepare('SELECT * FROM article;');
    $req->execute();

    return $req->fetchAll(PDO::FETCH_ASSOC);

}

public function fetchArticleById($id){
    $req = $this->bdd->prepare('SELECT * FROM article WHERE id = :id');
    $req->execute([':id' => $id]);

    return $req->fetch(PDO::FETCH_ASSOC);

}

public function deleteArticle($id){
    $req = $this->bdd->prepare('DELETE FROM article WHERE id = :id');
    $req->execute([':id' => $id]);
}



public function addArticle($libelle, $prixht, $prix, $categorie){
    $req = $this->bdd->prepare(
        'INSERT INTO article (libelle, prixht, prix, fk_categorie)
         VALUES (:libelle, :prixht, :prix, :categorie)');
    $req->execute([
        ':libelle' => $libelle,
        ':prixht' => $prixht,
        ':prix' => $prix,
        ':categorie' => $categorie
    ]);    
}

public function fetchCategorie() {
    $req =  $this->bdd->prepare('Select * FROM categorie;');
    $req->execute();
    return $req->fetchAll(PDO::FETCH_ASSOC);
}

public function fetchCategorieById($id){
    $req = $this->bdd->prepare('SELECT * FROM categorie WHERE id = :id');
    $req->execute([':id' => $id]);

    return $req->fetch(PDO::FETCH_ASSOC);

}

public function deleteCategorie($id){
    $req = $this->bdd->prepare('DELETE FROM categorie WHERE id = :id');
    $req->execute([':id' => $id]);
}

public function addCategorie($id, $nom, $descrip){
    $req = $this->bdd->prepare(
        'INSERT INTO categorie (id, nom, descrip)
         VALUES (:id, :nom, :descrip)');
    $req->execute([
        ':id' => $id,
        ':nom' => $nom,
        ':descrip' => $descrip
    ]);
    

}


}