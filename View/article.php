<?php
include '../View/header.php';

if (!isset($article) || !is_array($article)) {
  $article = [];
}

?>

<div class="container">

<div style="display: flex; flex-direction: column">
        <div style="display:flex;justify-content: end">
            <a href="../Controller/article.php?article=beforeAdd">
            <button class="btn btn-primary"> Catégories</button>
            </a>
        </div>

<div style="display: flex; flex-direction: column">
        <div style="display:flex;justify-content: end">
            <a href="../Controller/article.php?article=beforeAdd">
            <button class="btn btn-primary"> Ajouter un article</button>
            </a>
        </div>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Libelle</th>
        <th scope="col">Prix HT</th>
        <th scope="col">Prix TTC</th>
        <th scope="col">Catégories</th>
      </tr>
    </thead>
    <tbody>
      <?php

      if (is_array($article)) {

        foreach ($articles as $article) { ?>
          <tr>
            <td><?= $article['libelle']; ?></td>
            <td><?php echo $article['prixht']; ?></td>
            <td><?php echo $article['prix']; ?></td>
            <td><?php echo $article['fk_categorie']; ?></td>
            <td> <a href="../Controller/article.php?article=getOneById&id=<?= $article['id'] ?>">
            <button class="btn btn-primary">Voir Détails</button></a>
            <a href="../Controller/article.php?article=delete&id=<?= $article['id'] ?>">
            <button class="btn btn-danger">Danger</button></a></td>
            </a>
          </tr>
        <?php }
      } ?>
    </tbody>
  </table>
</div>