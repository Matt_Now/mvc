<?php
include '../View/header.php';

if (!isset($categorie) || !is_array($categorie)) {
    $categorie = [];
}

?>


<div style="display: flex; flex-direction: column">
        <div style="display:flex;justify-content: end">
            <a href="../Controller/categorie.php?categorie=beforeAdd">
            <button class="btn btn-primary"> Ajouter une categorie</button>
            </a>
        </div>

<div class="container">

  <table class="table">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">nom</th>
        <th scope="col">descrip</th>
      </tr>
    </thead>
    <tbody>
    <?php

if (is_array($categorie)) {

  foreach ($categorie as $categorie) { ?>
    <tr>
      <td><?= $categorie['id']; ?></td>
      <td><?php echo $categorie['nom']; ?></td>
      <td><?php echo $categorie['descrip']; ?></td>
      <td> <a href="../Controller/categorie.php?categorie=getOneById&id=<?= $categorie['id'] ?>">
      <button class="btn btn-primary">Voir Détails</button></a>
      <a href="../Controller/categorie.php?categorie=delete&id=<?= $categorie['id'] ?>">
      <button class="btn btn-danger">Danger</button></a></td>
      </a>
    </tr>
  <?php }
} ?>
</tbody>
</table>
</div>