<?php

include '../View/header.php';

?>

<div class="container mt-3">
<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title"><?= $article['libelle']?></h5>
    <p class="card-text">Prix HT : <?= $article['prixht']?> <br>
    Prix TTC : <?= $article['prix']?>
    </p>
  </div>
 </div> 
</div>